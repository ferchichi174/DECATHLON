import "./styles.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@vtmn/css-button/dist/index-with-vars.css';
import 'typeface-roboto';
import 'typeface-roboto-condensed';
import { VtmnButton } from '@vtmn/react';
import 'typeface-roboto';
import 'typeface-roboto-condensed';
import MyImage from './sad.png';

export default function App() {
  const [imageData, setImageData] = useState();
  const [token, seTtoken] = useState(null);
  const [preview, setPreview] = useState(null);
  const [names , setNames]= useState([])
  const [names1 , setNames1]= useState([])
    const [show, setShow] = useState(false);
    const [showA, setshowA] = useState(false);
    const handleClose = () => setShow(false);
    const handleClose1 = () => setshowA(false);
    const handleShow = () => setShow(true);


  useEffect(() => {
    if (!imageData) {
      setPreview(null);
      return;
    }

    // Blob - это объект, подобный File.
    // Интерфейс File основан на Blob, наследует функцию Blob и расширяется (доп. "name", "lastModified")
    // для поддержки локальных файлов на компьютере пользователя.
    const objectUrl = URL.createObjectURL(imageData);
    setPreview(objectUrl);

    // Браузер не может освободить память, занятую Blob-объектом.
    // Удаляеем внутреннюю ссылку на объект, что позволяет удалить его (если нет другой ссылки)
    // сборщику мусора, и память будет освобождена.
    return () => URL.revokeObjectURL(objectUrl);
  }, [imageData]);

  const handleFileInputChange = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      // error
      return;
    }
    setImageData(e.target.files[0]);

  };



    function equalIgnoreCase(str1, str2)
    {
        let i = 0;

        // length of first string
        let len1 = str1.length;

        // length of second string
        let len2 = str2.length;

        // if length is not same
        // simply return false since both string
        // can not be same if length is not equal
        if (len1 != len2)
            return false;

        // loop to match one by one
        // all characters of both string
        while (i < len1)
        {

            // if current characters of both string are same,
            // increase value of i to compare next character
            if (str1[i] == str2[i])
            {
                i++;
            }

                // if any character of first string
                // is some special character
                // or numeric character and
                // not same as corresponding character
            // of second string then return false
            else if (!((str1[i].charCodeAt() >= 'a'.charCodeAt() && str1[i].charCodeAt() <= 'z'.charCodeAt())
                || (str1[i].charCodeAt() >= 'A'.charCodeAt() && str1[i].charCodeAt() <= 'Z'.charCodeAt())))
            {
                return false;
            }

            // do the same for second string
            else if (!((str2[i].charCodeAt() >= 'a'.charCodeAt() && str2[i].charCodeAt() <= 'z'.charCodeAt())
                || (str2[i].charCodeAt() >= 'A'.charCodeAt() && str2[i].charCodeAt() <= 'Z'.charCodeAt())))
            {
                return false;
            }

                // this block of code will be executed
                // if characters of both strings
            // are of different cases
            else
            {
                // compare characters by ASCII value
                if (str1[i].charCodeAt() >= 'a'.charCodeAt() && str1[i].charCodeAt() <= 'z'.charCodeAt())
                {
                    if (str1[i].charCodeAt() - 32 != str2[i].charCodeAt())
                        return false;
                }

                else if (str1[i].charCodeAt() >= 'A'.charCodeAt() && str1[i].charCodeAt() <= 'Z'.charCodeAt())
                {
                    if (str1[i].charCodeAt() + 32 != str2[i].charCodeAt())
                        return false;
                }

                // if characters matched,
                // increase the value of i to compare next char
                i++;

            } // end of outer else block

        } // end of while loop

        // if all characters of the first string
        // are matched with corresponding
        // characters of the second string,
        // then return true
        return true;

    }
    const setImageAction =async  () => {
        console.log(imageData)
        const formData1 = new FormData();
        formData1.append(
            "grant_type",
            "client_credentials",
        );

        const  access_token1 ='QzBlZjlkNjAxZjkwMWZmMDdkNWUzYzg3YjVkMmM1YmIyOWMzMzk1MGU6U2lhaGZnRkE0NDFSMzliaEJkSEFJZUpOV3Y4MFNBMVpia21pRXV6ZkYya2RIb1ZHYnRuZWR0Qkl3NU9yaTVySQ==';
        const config1 = {
            headers: {
                'Content-Type':'application/x-www-form-urlencoded',
                'Authorization': 'Basic QzBlZjlkNjAxZjkwMWZmMDdkNWUzYzg3YjVkMmM1YmIyOWMzMzk1MGU6U2lhaGZnRkE0NDFSMzliaEJkSEFJZUpOV3Y4MFNBMVpia21pRXV6ZkYya2RIb1ZHYnRuZWR0Qkl3NU9yaTVySQ==',
            },
        };
        // const config = {
        //     headers: {
        //
        //         "Content-type": "application/json",
        //         'X-API-KEY': 'b161265e-774e-4c16-ae29-024078274571',
        //         'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Ik1BSU4iLCJwaS5hdG0iOiIyIn0.eyJzY29wZSI6W10sImNsaWVudF9pZCI6IkMwZWY5ZDYwMWY5MDFmZjA3ZDVlM2M4N2I1ZDJjNWJiMjljMzM5NTBlIiwiaXNzIjoiaWRwZGVjYXRobG9uIiwianRpIjoiSmd6NkdzbHlTMSIsInN1YiI6IkMwZWY5ZDYwMWY5MDFmZjA3ZDVlM2M4N2I1ZDJjNWJiMjljMzM5NTBlIiwib3JpZ2luIjoiY29ycG9yYXRlIiwiZXhwIjoxNjY5OTMwNzkxfQ.db-xhYaUP5uiAlXd9GADTK94HrJLtcNZNoixP_H-ZNqr8DxbagaHX-1NxFHlGQ0v0HLluoQ2F0KuUZC5yxncpZLmX_-N2TLlwts_S-jADfZUcUVkn8Lf80oG4fvBtXovxM0lyrwchLPYlCupXaSbyEF66-r-LcenhZszHKEaq1572IMqMrQsjUcBm9lrv9YOoSdQDuksUAGVljycxaNOuDf7l2u60efllzZjMP9Yw8WfCQcY-SP2PJEnaAUQVzUNYeC1qC7gkxdJk7LBs7z3REE7c3dXPlJrfqgn4Tl1CafhMvKjDCEAKUHRmbRcdO20-qfvkG0diJdVv1FsunjeKg',
        //
        //
        //     },
        //
        // };

            var tok
        var result = await axios.post('https://idpdecathlon.oxylane.com/as/token.oauth2', {
            'grant_type':'client_credentials'}, config1)

        console.log(tok)
        const formData = new FormData();
        formData.append(
            "file",
            imageData,
        );

        const config = {
            headers: {
                'x-api-key': 'b161265e-774e-4c16-ae29-024078274571',
                'Authorization': `Bearer ${result.data.access_token}` ,
            },
        };
        // const config = {
        //     headers: {
        //
        //         "Content-type": "application/json",
        //         'X-API-KEY': 'b161265e-774e-4c16-ae29-024078274571',
        //         'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Ik1BSU4iLCJwaS5hdG0iOiIyIn0.eyJzY29wZSI6W10sImNsaWVudF9pZCI6IkMwZWY5ZDYwMWY5MDFmZjA3ZDVlM2M4N2I1ZDJjNWJiMjljMzM5NTBlIiwiaXNzIjoiaWRwZGVjYXRobG9uIiwianRpIjoiSmd6NkdzbHlTMSIsInN1YiI6IkMwZWY5ZDYwMWY5MDFmZjA3ZDVlM2M4N2I1ZDJjNWJiMjljMzM5NTBlIiwib3JpZ2luIjoiY29ycG9yYXRlIiwiZXhwIjoxNjY5OTMwNzkxfQ.db-xhYaUP5uiAlXd9GADTK94HrJLtcNZNoixP_H-ZNqr8DxbagaHX-1NxFHlGQ0v0HLluoQ2F0KuUZC5yxncpZLmX_-N2TLlwts_S-jADfZUcUVkn8Lf80oG4fvBtXovxM0lyrwchLPYlCupXaSbyEF66-r-LcenhZszHKEaq1572IMqMrQsjUcBm9lrv9YOoSdQDuksUAGVljycxaNOuDf7l2u60efllzZjMP9Yw8WfCQcY-SP2PJEnaAUQVzUNYeC1qC7gkxdJk7LBs7z3REE7c3dXPlJrfqgn4Tl1CafhMvKjDCEAKUHRmbRcdO20-qfvkG0diJdVv1FsunjeKg',
        //
        //
        //     },
        //
        // };

        axios.post('https://cors-anywhere.herokuapp.com/https://api.decathlon.net/sport_vision_api/v1/sportclassifier/predict/', formData, config)
            .then((res) => {

                var resultNames = res.data.data.sport.map(item => {
                    return item.name
                })
                console.log(resultNames)

                setNames(resultNames)
                const api =names;
                console.log(resultNames[0])

                const jo = ['Athlétisme','Aviron','Badminton','Baseball Softball','Basketball','Basketball 3x3','Boxe','Breaking','Canoë Slalom','Canoë Sprint','Cyclisme BMX Freestyle','Cyclisme BMX Racing','Cyclisme sur piste','Cyclisme sur route','Cyclisme VTT','Escalade Sportive','Escrime','Golf','Gymnastique Artistique','Gymnastique Rythmique','Haltérophilie','Handball','Hockey sur Gazon','Judo','Karaté','Lutte','Natation','Natation Artistique','Natation, marathon','Pentathlon Moderne','Plongeon','Rugby à 7','Skateboard','Sports Équestres','Surf','Taekwondo','Tennis','Tennis de Table','Tir','Tir à LArc','Trampoline','Triathlon','Voile','Volleyball','Volleyball de plage','Water-Polo']
                var test = false;
                jo.forEach((joly, index) => {
                    resultNames.forEach((apil, index) => {
                        if(equalIgnoreCase(apil,joly) ){
                            test = true
                        }
                    })


                });

       // ---------------------------------------


                const config3 = {
                    headers: {
                        'x-api-key': 'b161265e-774e-4c16-ae29-024078274571',
                        'Authorization': `Bearer ${result.data.access_token}` ,
                    },
                };


                axios.post('https://cors-anywhere.herokuapp.com/https://api.decathlon.net/sport_vision_api/v1/productretrieval/predict/',formData, config3)
                    .then((res) => {
                        var resultNames1 = res.data.data.map(item => {
                            console.log(item.image_url)
                            return {url : item.image_url , name : item.product.name}
                        })
                        setNames1(resultNames1)
                        console.log(resultNames1)
                    })
                    .catch((error) => {
                        console.error(error)
                    })



                console.log(test)

                if (test == true ){


                    setShow(true)



                }
                else if (test == false){
                    setshowA(true)
                }
            })
            .catch((error) => {
                console.error(error)
            })
        // const api = 'hjghj';




    }



  return (
    <div className="App" style={{ backgroundColor:"#BEDEEF"}}>
        <svg width="200" height="50" viewBox="0 0 200 50" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="200" height="50" fill="white"/><path d="M0 50H200V0H0V50Z" fill="#0082C3"/><path d="M63.815 38.2511C63.299 38.2511 62.77 38.225 62.244 38.174C59.2349 37.8655 56.4092 36.5812 54.198 34.517C51.5645 31.995 50.1168 28.4792 50.211 24.834C50.3192 21.9503 51.3129 19.17 53.057 16.871C54.1718 15.4002 55.6 14.1961 57.238 13.346C59.2746 12.3267 61.5186 11.7902 63.796 11.778C63.896 11.778 64.004 11.778 64.106 11.778C68.1293 11.8068 71.9377 13.5977 74.526 16.678L74.586 16.755L71.225 22.141L71.001 21.875L70.844 21.701L70.538 21.379L70.288 21.132C69.125 20.014 66.745 18.137 63.871 18.137H63.738C60.0158 18.2013 57.0301 21.2333 57.023 24.956C57.023 28.056 58.887 31.697 64.139 31.778H64.216C67.27 31.778 69.266 29.535 70.183 28.198L70.383 27.898C70.494 27.725 70.752 27.298 71.174 26.598L79.675 12.287H86.553V37.677H80.223V33.868L75.18 33.846H74.507L74.323 33.854L74.291 33.86L74.155 34.01L73.993 34.177L73.764 34.399L73.572 34.576L73.35 34.776L73.1 34.986C70.4717 37.1082 67.1931 38.2612 63.815 38.2511ZM80.353 22.806L76.96 28.556H80.353V22.806ZM151.769 38.239C148.169 38.322 144.699 36.8956 142.198 34.305V37.694H127.988V12.291H134.526V32.08H140.434C139.075 29.8728 138.379 27.3223 138.426 24.731C138.421 23.0097 138.765 21.3053 139.438 19.721C140.12 18.1304 141.115 16.693 142.363 15.494C144.879 13.0275 148.284 11.6799 151.807 11.756C155.269 11.7561 158.593 13.109 161.071 15.526C162.634 17.0195 163.803 18.8777 164.471 20.934V12.286H170.413L180.291 26.396V12.286H186.878V37.694H181.091L171.058 23.539V37.694H164.475V28.985C163.869 30.9698 162.789 32.7772 161.328 34.251C158.817 36.8254 155.365 38.2655 151.769 38.239ZM151.843 18.077C150.018 18.0618 148.26 18.7663 146.95 20.038C145.665 21.2888 144.943 23.0078 144.95 24.801C144.942 26.6962 145.686 28.5172 147.018 29.865C148.267 31.1496 149.975 31.8849 151.767 31.909C153.563 31.922 155.286 31.2027 156.54 29.917C157.844 28.5363 158.555 26.6997 158.52 24.801C158.528 21.1027 155.541 18.0946 151.843 18.077ZM112.638 37.695H106.068V12.284H112.638V21.932H119.404V12.284H125.979V37.693H119.404V27.972H112.638V37.695ZM99.606 37.695H93.038V18.35H87.926V12.276H104.712V18.35H99.612V37.693L99.606 37.695ZM22.123 37.695H12.038V12.287H21.226C25.44 12.287 28.436 12.787 31.276 15.322C33.3402 17.1998 34.6659 19.7532 35.014 22.522V12.287H49.638V17.876L41.592 17.885V22.036L48.559 22.03V27.494L41.592 27.501V32.106H49.638V37.677H35.013V27.548C34.6082 30.2348 33.3004 32.7037 31.305 34.548C28.438 37.193 25.865 37.694 22.126 37.694L22.123 37.695ZM18.615 17.75V32.114H21.256C23.809 32.114 25.638 31.548 26.848 30.384C28.058 29.22 28.666 27.414 28.666 24.904C28.666 20.291 26.024 17.75 21.226 17.75H18.615Z" fill="#FEFEFE"/></svg>
      <h1 style={{ fontFamily:"Roboto"}} class="vtmn-typo_text-2">Est ce que c'est une image de jeux Olympique ?</h1>

      <input type="file" onChange={handleFileInputChange} />
      <h2 style={ { fontFamily:"Roboto"}}>Preview <ins>{imageData?.name}</ins>
      </h2>
      <div className="imgContainer">
        {imageData ? (
          <img src={preview} alt="preview" />
        ) : (
          <p>
            <i>Please, inter the image</i>
          </p>
        )}
      </div>
        <div className="d-flex justify-content-center" style={{ position:"block"}}>
            <VtmnButton
                style={{ marginTop:"30px"}}
                iconAlone={null}
                iconLeft={null}
                iconRight={null}
                size="medium"
                variant="primary"
                onClick={setImageAction}
            >
                Tester
            </VtmnButton>

        </div>


        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
                <Modal.Body><h2>Woohoo, c'est un sport des JO !</h2>


                </Modal.Body>
            <Modal.Body>   <h4 style={{ textAlign:'Center'}}>Des produits pour ce sport</h4>


            </Modal.Body>
            <Modal.Footer>

                <div style={{
                    display:"flex",
                    flexWrap : "wrap"
                }}>
                    {names1.map(item => {
                        return <div style={{ margin:"20px"}} >
                            <img  src={item.url} style={{width : "100px"}} />
                            <p>{item.name.substring(0,10) }</p>
                        </div>
                    })}
                </div>
            </Modal.Footer>
        </Modal>
        <Modal show={showA} onHide={handleClose1}>
            <Modal.Header closeButton>
                <Modal.Title> <img  style={{ width:"100px",marginLeft:"173px"}} src={MyImage} /></Modal.Title>
            </Modal.Header>
            <Modal.Body><h5>Oops ce n'est pas un sport des jeux Olympiques</h5></Modal.Body>
        </Modal>
    </div>

  );
}
